package com.example.ambro.bicingapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.osmdroid.api.IMapController;
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer;
import org.osmdroid.views.overlay.MapEventsOverlay;

import org.osmdroid.views.overlay.Marker;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.MinimapOverlay;
import org.osmdroid.views.overlay.ScaleBarOverlay;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static android.content.ContentValues.TAG;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private MapView map;
    private MyLocationNewOverlay myLocationOverlay;
    private ScaleBarOverlay mScaleBarOverlay;
    private CompassOverlay mCompassOverlay;
    private IMapController mapController;
    private RotationGestureOverlay mRotationGestureOverlay;
    private Context baseContext;
    private static final int RC_SIGN_IN = 123;
    private RadiusMarkerClusterer puntosMarkers;
    private FirebaseAuth mAuth;
    private GoogleSignInClient mGoogleSignInClient;
    private Button camera;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_main, container, false);
        map = view.findViewById(R.id.map);
        camera = view.findViewById(R.id.camera);
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null) {
            doLogin();
        }

        Context ctx = getContext();
        puntosMarkers = new RadiusMarkerClusterer(ctx);
        checkSignIn();
        org.osmdroid.config.Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));


        initializeMap();
        setZoom();
        setOverlays();
        setRotate();
        setCompass();

        map.invalidate();

        camera.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View view) {
                dispatchTakePictureIntent();
            }
        });

        return view;
    }

    private void setCompass() {
        this.mCompassOverlay = new CompassOverlay(this.getContext(), new InternalCompassOrientationProvider(this.getContext()), map);
        this.mCompassOverlay.enableCompass();
        map.getOverlays().add(this.mCompassOverlay);
    }

    private void setRotate() {
        mRotationGestureOverlay = new RotationGestureOverlay(this.getContext(), map);
        mRotationGestureOverlay.setEnabled(true);
        map.setMultiTouchControls(true);
        map.getOverlays().add(this.mRotationGestureOverlay);
    }

    private void getPuntosInfo() {
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("data/punto");

        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                                        puntos puntos = postSnapshot.getValue(puntos.class);

                                        Marker marker = new Marker(map);

                                        GeoPoint point = new GeoPoint(
                                                        Double.parseDouble(puntos.getLat()),
                                                        Double.parseDouble(puntos.getLon())
                                                        );
                                        marker.setPosition(point);

                                                marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
                                        marker.setIcon(getResources().getDrawable(R.drawable.bonuspack_bubble));
                                        marker.setTitle(puntos.getName());
                                        marker.setAlpha(0.6f);

                    puntosMarkers.add(marker);
                }
                                puntosMarkers.invalidate();
                                map.invalidate();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void initializeMap() {
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setTilesScaledToDpi(true);

        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
    }
    private void setZoom() {
        //  Setteamos el zoom al mismo nivel y ajustamos la posición a un geopunto
        mapController = map.getController();
        mapController.setZoom(14);
    }

    private void doLogin(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this.getContext(), gso);
        mAuth = FirebaseAuth.getInstance();
        signIn();
    }

    private void startActivityForResult(Intent build) {

    }

    private void setupMarkerOverlay() {
        RadiusMarkerClusterer stationMarkers = new RadiusMarkerClusterer(this.getContext());

        Drawable clusterIconD = getResources().getDrawable(R.drawable.bonuspack_bubble);
        Bitmap clusterIcon = ((BitmapDrawable) clusterIconD).getBitmap();

        stationMarkers.setIcon(clusterIcon);
        stationMarkers.setRadius(100);

        stationMarkers.invalidate();
        map.getOverlays().add(stationMarkers);

    }

    private void checkSignIn() {
        if (mAuth.getCurrentUser() == null) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();

            mGoogleSignInClient = GoogleSignIn.getClient(this.getContext(), gso);
            mAuth = FirebaseAuth.getInstance();
            signIn();
        } else {
            Log.d("No usuario", "checkSignIn: ");
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void putMarkers() {
        setupMarkerOverlay();
        getPuntosInfo();
        //getStationInfo();
    }

    private void setOverlays() {
        final DisplayMetrics dm = getResources().getDisplayMetrics();

        this.myLocationOverlay = new MyLocationNewOverlay(new GpsMyLocationProvider(this.getContext()),map);
        this.myLocationOverlay.enableMyLocation();
        myLocationOverlay.runOnFirstFix(new Runnable() {
        public void run() {
            //GeoPoint startPoint = new GeoPoint(myLocationOverlay.getMyLocation());

            mapController.animateTo(myLocationOverlay.getMyLocation());
            //mapController.setCenter(startPoint);
                }
        });
/*
        mRotationGestureOverlay = new RotationGestureOverlay(map);
        mRotationGestureOverlay.setEnabled(true);
        map.setMultiTouchControls(true);
        map.getOverlays().add(this.mRotationGestureOverlay);
*/
        mScaleBarOverlay = new ScaleBarOverlay(map);
        mScaleBarOverlay.setCentred(true);
        mScaleBarOverlay.setScaleBarOffset(dm.widthPixels / 2, 10);

        //puntosMarkers = new Marker.OnMarkerClickListener()


        /*mCompassOverlay = new CompassOverlay(
        getContext(),new InternalCompassOrientationProvider(getContext()),map);
        mCompassOverlay.enableCompass();

        puntosMarkers = new RadiusMarkerClusterer(getContext());


        Drawable clusterIconD = getResources().getDrawable(R.drawable.btn_moreinfo);

        puntosMarkers.setIcon();
        puntosMarkers.setRadius(100);

        puntosMarkers.invalidate();*/

        puntosMarkers.clusterer(map);
        getPuntosInfo();

        map.getOverlays().add(puntosMarkers);
        map.getOverlays().add(myLocationOverlay);
        map.getOverlays().add(this.mScaleBarOverlay);
        //map.getOverlays().add(this.mRotationGestureOverlay);
        //map.getOverlays().add(this.puntosMarkers);

    }

    String mCurrentPhotoPath;

    private File createImageFile() throws IOException {
        // Create an image file name
        String imageFileName = "JPEG_" + "BicingApp" + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }

    static final int REQUEST_TAKE_PHOTO = 1;
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(this.getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

}
